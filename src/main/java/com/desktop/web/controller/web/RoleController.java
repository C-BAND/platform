/**
 * $Revision$
 * $Date$
 *
 * Copyright (C) https://gitee.com/baibaiclouds/platform loon. All rights reserved.
 * <p>
 * This software is the confidential and proprietary information of loon.
 * You shall not disclose such Confidential Information and shall use it only
 * in accordance with the terms of the agreements you entered into with loon.
 * 
 * Modified history:
 *   Loon  2019年11月2日 下午11:58:03  created
 */
package com.desktop.web.controller.web;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.desktop.web.core.aop.RightTarget;
import com.desktop.web.core.exception.BusinessException;
import com.desktop.web.core.utils.RequestUtil;
import com.desktop.web.core.utils.Util;
import com.desktop.web.core.web.Result;
import com.desktop.web.service.node.NodeService;
import com.desktop.web.service.role.RoleService;
import com.desktop.web.uda.entity.Node;
import com.desktop.web.uda.entity.Role;
import com.desktop.web.uda.entity.User;

/**
 * 
 *
 * @author baibai
 */
@Controller
public class RoleController extends BaseWebController {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private RoleService roleService;

    @Autowired
    private NodeService nodeService;

    @RequestMapping(value = "/webapi/role/list/get", method = {RequestMethod.GET})
    @ResponseBody
    @RightTarget(value = "team_role_manager")
    public Object getListRole(HttpServletRequest request) {

        try {
            List<Role> roles = roleService.getRoleList();
            return Result.Success(roles);
        } catch (BusinessException e) {
            logger.error(e.getMessage(), e);
            return Result.Error(e.getMessage());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return Result.Error();
        }
    }

    @RequestMapping(value = "/webapi/role/add", method = {RequestMethod.POST})
    @ResponseBody
    @RightTarget(value = "team_role_manager")
    public Object addRole(HttpServletRequest request) {

        try {
            Map<String, String> params = RequestUtil.getBody(request);
            if (params == null || params.isEmpty()) {
                throw new BusinessException("参数为空");
            }

            Role role = roleService.addRole(params.get("title"));
            return Result.Success(role);
        } catch (BusinessException e) {
            logger.error(e.getMessage(), e);
            return Result.Error(e.getMessage());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return Result.Error();
        }
    }

    @RequestMapping(value = "/webapi/role/update", method = {RequestMethod.POST})
    @ResponseBody
    @RightTarget(value = "team_role_manager")
    public Object updateRole(HttpServletRequest request) {

        try {
            Map<String, String> params = RequestUtil.getBody(request);
            if (params == null || params.isEmpty()) {
                throw new BusinessException("参数为空");
            }

            Role role = new Role();
            role.setTitle(params.get("title"));
            role.setId(Long.valueOf(params.get("id")));
            roleService.updateRole(role);
            return Result.Success();
        } catch (BusinessException e) {
            logger.error(e.getMessage(), e);
            return Result.Error(e.getMessage());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return Result.Error();
        }
    }

    @RequestMapping(value = "/webapi/role/delete", method = {RequestMethod.POST})
    @ResponseBody
    @RightTarget(value = "team_role_manager")
    public Object deleteRole(HttpServletRequest request) {

        try {
            Map<String, String> params = RequestUtil.getBody(request);
            if (params == null || params.isEmpty()) {
                throw new BusinessException("参数为空");
            }

            roleService.deleteRole(Long.valueOf(params.get("id")));
            return Result.Success();
        } catch (BusinessException e) {
            logger.error(e.getMessage(), e);
            return Result.Error(e.getMessage());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return Result.Error();
        }
    }

    @SuppressWarnings("rawtypes")
    @RequestMapping(value = "/webapi/role/right/update", method = {RequestMethod.POST})
    @ResponseBody
    @RightTarget(value = "team_role_manager")
    public Object updateRightRole(HttpServletRequest request) {

        try {

            Map<String, Object> params = RequestUtil.getBody2(request);
            if (params == null || params.isEmpty()) {
                throw new BusinessException("参数为空");
            }
            @SuppressWarnings("unchecked")
            Map<String, Object> rights = (Map) params.get("rights");
            roleService.updateRightRole(Long.valueOf(params.get("id").toString()), rights);
            return Result.Success();
        } catch (BusinessException e) {
            logger.error(e.getMessage(), e);
            return Result.Error(e.getMessage());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return Result.Error();
        }
    }

    @RequestMapping(value = "/webapi/role/right/list/get", method = {RequestMethod.GET})
    @ResponseBody
    @RightTarget(value = "team_role_manager")
    public Object getRightRoleList(HttpServletRequest request) {

        try {

            List<String> list = roleService.getRightTargetListByRoleid(Long.valueOf(request.getParameter("roleId")));
            return Result.Success(list);
        } catch (BusinessException e) {
            logger.error(e.getMessage(), e);
            return Result.Error(e.getMessage());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return Result.Error();
        }
    }

    @RequestMapping(value = "/webapi/role/user/list/get", method = {RequestMethod.GET})
    @ResponseBody
    @RightTarget(value = "team_role_manager")
    public Object getRoleUserList(HttpServletRequest request) {

        try {
            List<User> list = roleService.getRoleUserList(Long.valueOf(request.getParameter("roleId")));
            return Result.Success(list);
        } catch (BusinessException e) {
            logger.error(e.getMessage(), e);
            return Result.Error(e.getMessage());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return Result.Error();
        }
    }

    @RequestMapping(value = "/webapi/role/user/add", method = {RequestMethod.POST})
    @ResponseBody
    @RightTarget(value = "team_role_manager")
    public Object addRoleUser(HttpServletRequest request) {

        try {

            Map<String, Object> params = RequestUtil.getBody2(request);
            if (params == null || params.isEmpty()) {
                throw new BusinessException("参数为空");
            }

            Long roleId = Long.valueOf(params.get("roleId").toString());
            List<Long> uids = Util.toArrayLong(params.get("uids").toString());

            roleService.addRoleUserList(roleId, uids);
            return Result.Success();
        } catch (BusinessException e) {
            logger.error(e.getMessage(), e);
            return Result.Error(e.getMessage());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return Result.Error();
        }
    }

    @RequestMapping(value = "/webapi/role/user/delete", method = {RequestMethod.POST})
    @ResponseBody
    @RightTarget(value = "team_role_manager")
    public Object delRoleUser(HttpServletRequest request) {

        try {

            Map<String, Object> params = RequestUtil.getBody2(request);
            if (params == null || params.isEmpty()) {
                throw new BusinessException("参数为空");
            }

            Long roleId = Long.valueOf(params.get("roleId").toString());
            Long uid = Long.valueOf(params.get("uid").toString());

            roleService.delRoleUser(roleId, uid);
            return Result.Success();
        } catch (BusinessException e) {
            logger.error(e.getMessage(), e);
            return Result.Error(e.getMessage());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return Result.Error();
        }
    }

    @RequestMapping(value = "/webapi/role/node/list/get", method = {RequestMethod.GET})
    @ResponseBody
    @RightTarget(value = "team_role_manager")
    public Object getRoleNodeList(HttpServletRequest request) {

        try {
            List<Node> list = roleService.getRoleNodeList(Long.valueOf(request.getParameter("roleId")));
            return Result.Success(list);
        } catch (BusinessException e) {
            logger.error(e.getMessage(), e);
            return Result.Error(e.getMessage());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return Result.Error();
        }
    }

    @RequestMapping(value = "/webapi/role/node/update", method = {RequestMethod.POST})
    @ResponseBody
    @RightTarget(value = "team_role_manager")
    public Object updateRoleNodeList(HttpServletRequest request) {

        try {
            Map<String, Object> params = RequestUtil.getBody2(request);
            if (params == null || params.isEmpty()) {
                throw new BusinessException("参数为空");
            }

            Long roleId = Long.valueOf(params.get("roleId").toString());
            List<Long> ids = Util.toArrayLong(params.get("ids").toString());

            for (Long item : ids) {
                nodeService.checkNodeByCuruser(item);
            }

            roleService.updateRoleNodeList(roleId, ids);
            return Result.Success();
        } catch (BusinessException e) {
            logger.error(e.getMessage(), e);
            return Result.Error(e.getMessage());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return Result.Error();
        }
    }

    @RequestMapping(value = "/webapi/role/node/delete", method = {RequestMethod.POST})
    @ResponseBody
    @RightTarget(value = "team_role_manager")
    public Object delRoleNode(HttpServletRequest request) {

        try {

            Map<String, Object> params = RequestUtil.getBody2(request);
            if (params == null || params.isEmpty()) {
                throw new BusinessException("参数为空");
            }

            Long roleId = Long.valueOf(params.get("roleId").toString());
            Long nodeId = Long.valueOf(params.get("nodeId").toString());
            nodeService.checkNodeByCuruser(nodeId);

            roleService.delRoleNode(roleId, nodeId);
            return Result.Success();
        } catch (BusinessException e) {
            logger.error(e.getMessage(), e);
            return Result.Error(e.getMessage());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return Result.Error();
        }
    }
}
