/**
 * $Revision$
 * $Date$
 *
 * Copyright (C) https://gitee.com/baibaiclouds/platform . All rights reserved.
 * <p>
 * This software is the confidential and proprietary information of .
 * You shall not disclose such Confidential Information and shall use it only
 * in accordance with the terms of the agreements you entered into with .
 * 
 * Modified history:
 *   baibai  2021年4月24日 下午6:04:49  created
 */
package com.desktop.web.config;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.config.annotation.InterceptorRegistration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.desktop.web.web.WebAuthHandlerInterceptor;

/**
 * 
 *
 * @author baibai
 */
@Configuration
public class WebMvcConfig implements WebMvcConfigurer, InitializingBean {

    @Value("${http.white.url.active}")
    private List<String> whiteActiveIndexs;

    @Autowired
    private Environment env;

    private List<String> whiteUrls = new ArrayList<String>();

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        InterceptorRegistration interceptorRegistration = registry.addInterceptor(new WebAuthHandlerInterceptor());
        interceptorRegistration.addPathPatterns("/webapi/**");
        interceptorRegistration.excludePathPatterns(whiteUrls);
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/static/**").addResourceLocations("classpath:/static/");
        registry.addResourceHandler("/web/**").addResourceLocations("classpath:/web/");
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        for (String index : whiteActiveIndexs) {
            String url = env.getProperty("http.white.url.target." + index);
            if (StringUtils.isEmpty(url)) {
                continue;
            }
            whiteUrls.add(url);
        }
    }

}
