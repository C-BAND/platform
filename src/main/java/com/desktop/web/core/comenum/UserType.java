/**
 * $Revision$
 * $Date$
 *
 * Copyright (C) https://gitee.com/baibaiclouds/platform baibai. All rights reserved.
 * <p>
 * This software is the confidential and proprietary information of baibai.
 * You shall not disclose such Confidential Information and shall use it only
 * in accordance with the terms of the agreements you entered into with baibai.
 * 
 * Modified history:
 *   baibai  2020年2月2日 下午7:08:00  created
 */
package com.desktop.web.core.comenum;

/**
 * 
 *
 * @author baibai
 */
public enum UserType {
    SOLE, ENTERPRISE
}
